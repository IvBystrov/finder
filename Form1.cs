﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

namespace Finder
{
    public partial class Form1 : Form
    {
        ToolStripLabel infoLabel;                               //Для вывода в строке состояния
        String selectedPath;                                    //Путь к выбранной директории
        String fileName;                                        //Шаблон поиска
        String textForFind;                                     //Текст, содержащийся в файле
        helperToAddNode helper;
        cleanerForNodes cleaner;
        updaterForStatus updater;
        Timer timer;                                            //Таймер для отсчета времени

        public delegate void helperToAddNode(String str);       //Для вывода в TreeView1 из фонового потока
        public delegate void cleanerForNodes();                 //Для очистки TreeView1 из фонового потока
        public delegate void updaterForStatus(String str);      //Для обновления строки состояния из фонового потока

        public Form1()
        {
            InitializeComponent();            
            infoLabel = new ToolStripLabel();
            
            statusStrip1.Items.Add(infoLabel);
            helper = new helperToAddNode(AddNode);
            cleaner = new cleanerForNodes(ClearTree);
            updater = new updaterForStatus(UpdateStatusBar);

            timer = new Timer();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reload();
            selectedPath = Properties.Settings.Default["Path"].ToString();
            folderBrowserDialog1.SelectedPath = selectedPath;
            textBox2.Text = Properties.Settings.Default["Name"].ToString();
            textBox1.Text = Properties.Settings.Default["Text"].ToString();
            infoLabel.Text = "Папка: " + selectedPath;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.Reload();
            Properties.Settings.Default["Path"] = selectedPath;
            Properties.Settings.Default["Name"] = fileName;
            Properties.Settings.Default["Text"] = textForFind;
            Properties.Settings.Default.Save();
            backgroundWorker1.CancelAsync();            //Остановка фонового потока перед закрытием формы
        }

        //Выбор стартовой директории
        private void button1_Click(object sender, EventArgs e)
        {            
            folderBrowserDialog1.ShowDialog();
            selectedPath = folderBrowserDialog1.SelectedPath;
            infoLabel.Text = "Папка: " + selectedPath;
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void AddNode(String str)
        {
            treeView1.Nodes.Add(str);
        }
        private void ClearTree()
        {
            treeView1.Nodes.Clear();
        }
        private void UpdateStatusBar(String str)
        {
            infoLabel.Text = str;
        }

        //Поиск в фоновом потоке и отображение результатов поиска
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {

            uint counter = 0;                                       //Счётчик обработанных файлов
            String status;

            timer.Start(); 

            fileName = textBox2.Text;
            textForFind = textBox1.Text;
            Invoke(cleaner);            

            foreach (string findedFile in Directory.EnumerateFiles(selectedPath, fileName,
                SearchOption.AllDirectories))                       //Поиск файла по шаблону имени
            {
                status = "";
                FileInfo FI;
                FI = new FileInfo(findedFile);                      //По полному пути к файлу создаём объект класса FileInfo

                status += "Обрабатывается: " + findedFile + " Обработано: " + Convert.ToString(counter) + " Время: " + timer.Interval.ToString() + " мс";
                Invoke(updater, status); 

                if(findedFile.Length > 0 && treeView1.Nodes.Count == 0)
                    Invoke(helper, selectedPath);

                try
                {                              
                    string str = File.ReadAllText(FI.FullName);     //Поиск по содержимому файла
                    if (str.Contains(textForFind))
                    {
                        
                        Invoke(helper, FI.Name);                    //Найденный результат выводим в treeView1
                    }                        
                }
                catch //Слишком длинное имя файла
                {
                    continue;
                }
                counter++;

            }            

        }

    }
}
